<?php

use Faker\Generator as Faker;

$factory->define(App\Pet::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});

$factory->state(App\Pet::class, 'dog', function (Faker $faker) {
  return [
    'pet_type_id' => function () {
      return App\PetType::where('name', 'Dog')->first()->id;
    }
  ];
});

$factory->state(App\Pet::class, 'cat', function (Faker $faker) {
  return [
    'pet_type_id' => function () {
      return App\PetType::where('name', 'Cat')->first()->id;
    }
  ];
});

$factory->state(App\Pet::class, 'smallParrot', function (Faker $faker) {
  return [
    'pet_type_id' => function () {
      return App\PetType::where('name', 'Small Parrot')->first()->id;
    }
  ];
});
