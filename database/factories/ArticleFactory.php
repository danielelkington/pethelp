<?php

use Faker\Generator as Faker;

$factory->define(App\Article::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(255),
        'content' => $faker->randomHtml(2, 3),
        'user_id' => function() {
          return factory(App\User::class)->create()->id;
        }
    ];
});
