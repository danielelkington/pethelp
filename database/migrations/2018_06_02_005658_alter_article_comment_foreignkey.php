<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterArticleCommentForeignkey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function($table){
          $table->dropForeign('comments_article_id_foreign');

          $table->foreign('article_id')->references('id')->on('articles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('comments', function($table){
        $table->dropForeign('comments_article_id_foreign');

        $table->foreign('article_id')->references('id')->on('articles');
      });
    }
}
