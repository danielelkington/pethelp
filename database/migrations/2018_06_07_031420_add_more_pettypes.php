<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Log;

class AddMorePettypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //Softbills
      $birdId = DB::table('pet_types')->where('name', 'Bird')
        ->value('id');
      $softbillId = DB::table('pet_types')->insertGetId(
        ['parent_id' => $birdId, 'name' => 'Softbill Bird']
      );
      DB::table('pet_types')->insert([
        ['parent_id' => $softbillId, 'name' => 'Crow'],
        ['parent_id' => $softbillId, 'name' => 'Raven'],
        ['parent_id' => $softbillId, 'name' => 'Mynah'],
        ['parent_id' => $softbillId, 'name' => 'Toucan']
      ]);

      //Small mammal
      $smallMammalId = DB::table('pet_types')->insertGetId(
        ['name' => 'Small Mammal']
      );
      DB::table('pet_types')->insert([
        ['parent_id' => $smallMammalId, 'name' => 'Guinea Pig'],
        ['parent_id' => $smallMammalId, 'name' => 'Hamster'],
        ['parent_id' => $smallMammalId, 'name' => 'Rabbit'],
        ['parent_id' => $smallMammalId, 'name' => 'Ferret'],
        ['parent_id' => $smallMammalId, 'name' => 'Chinchilla'],
        ['parent_id' => $smallMammalId, 'name' => 'Mouse'],
        ['parent_id' => $smallMammalId, 'name' => 'Rat'],
        ['parent_id' => $smallMammalId, 'name' => 'Gerbil'],
        ['parent_id' => $smallMammalId, 'name' => 'Pygmy hedgehog'],
        ['parent_id' => $smallMammalId, 'name' => 'Sugar glider']
      ]);

      //Rabbits
      $rabbitId = DB::table('pet_types')->where('name', 'Rabbit')
        ->value('id');
        DB::table('pet_types')->insert([
          ['parent_id' => $rabbitId, 'name' => 'American Rabbit'],
          ['parent_id' => $rabbitId, 'name' => 'American Sable'],
          ['parent_id' => $rabbitId, 'name' => 'Angora Rabbit'],
          ['parent_id' => $rabbitId, 'name' => 'Belgian Hare'],
          ['parent_id' => $rabbitId, 'name' => 'Beveren'],
          ['parent_id' => $rabbitId, 'name' => 'Blanc De Hotot'],
          ['parent_id' => $rabbitId, 'name' => 'Californian Rabbit'],
          ['parent_id' => $rabbitId, 'name' => 'Cashmere Lop Rabbit'],
          ['parent_id' => $rabbitId, 'name' => 'Dutch Rabbit'],
          ['parent_id' => $rabbitId, 'name' => 'Dwarf Lop'],
          ['parent_id' => $rabbitId, 'name' => 'Dwart Angora'],
          ['parent_id' => $rabbitId, 'name' => 'Holland Lop'],
          ['parent_id' => $rabbitId, 'name' => 'Rex Rabbit'],
          ['parent_id' => $rabbitId, 'name' => 'Netherland Dwarf'],
          ['parent_id' => $rabbitId, 'name' => 'French Lop']
        ]);

      //Sea creatures
      $seaCreatureId = DB::table('pet_types')->insertGetId(
        ['name' => 'Sea Creature']
      );
      DB::table('pet_types')->insert([
        ['parent_id' =>$seaCreatureId, 'name' => 'Fish'],
        ['parent_id' => $seaCreatureId, 'name' => 'Crayfish'],
        ['parent_id' => $seaCreatureId, 'name' => 'Frog'],
        ['parent_id' => $seaCreatureId, 'name' => 'Toad'],
        ['parent_id' => $seaCreatureId, 'name' => 'Crab'],
        ['parent_id' => $seaCreatureId, 'name' => 'Sea Monkey'],
        ['parent_id' => $seaCreatureId, 'name' => 'Shrimp'],
        ['parent_id' => $seaCreatureId, 'name' => 'Newt'],
        ['parent_id' => $seaCreatureId, 'name' => 'Axolotl'],
        ['parent_id' => $seaCreatureId, 'name' => 'Octopus']
      ]);
      $fishId = DB::table('pet_types')->where('name', 'Fish')
        ->value('id');
      DB::table('pet_types')->insert([
        ['parent_id' => $fishId, 'name' => 'Goldfish'],
        ['parent_id' => $fishId, 'name' => 'Betta fish'],
        ['parent_id' => $fishId, 'name' => 'Guppie'],
        ['parent_id' => $fishId, 'name' => 'White Cloud Minnow'],
        ['parent_id' => $fishId, 'name' => 'Tetra'],
        ['parent_id' => $fishId, 'name' => 'Salt and Pepper Corydoras'],
        ['parent_id' => $fishId, 'name' => 'Zebra Danios'],
        ['parent_id' => $fishId, 'name' => 'Oscar'],
        ['parent_id' => $fishId, 'name' => 'Mollie'],
        ['parent_id' => $fishId, 'name' => 'Platie'],
        ['parent_id' => $fishId, 'name' => 'Barb'],
        ['parent_id' => $fishId, 'name' => 'Gourami'],
        ['parent_id' => $fishId, 'name' => 'Swordtail'],
        ['parent_id' => $fishId, 'name' => 'Discus'],
        ['parent_id' => $fishId, 'name' => 'Killifish'],
        ['parent_id' => $fishId, 'name' => 'Bettas'],
        ['parent_id' => $fishId, 'name' => 'Plecostomus'],
        ['parent_id' => $fishId, 'name' => 'Rainbowfish'],
        ['parent_id' => $fishId, 'name' => 'Catfish'],
        ['parent_id' => $fishId, 'name' => 'Angelfish'],
        ['parent_id' => $fishId, 'name' => 'Plecostomus']
      ]);

      //Reptiles
      $reptileId = DB::table('pet_types')->insertGetId(
        ['name' => 'Reptile']
      );
      DB::table('pet_types')->insert([
        ['parent_id' => $reptileId, 'name' => 'Lizard'],
        ['parent_id' => $reptileId, 'name' => 'Turtle'],
        ['parent_id' => $reptileId, 'name' => 'Tortise'],
        ['parent_id' => $reptileId, 'name' => 'Snake']
      ]);

      //Lizard
      $lizardId = DB::table('pet_types')->where('name', 'Lizard')
        ->value('id');
      DB::table('pet_types')->insert([
        ['parent_id' => $lizardId, 'name' => 'Gecko'],
        ['parent_id' => $lizardId, 'name' => 'Ackie'],
        ['parent_id' => $lizardId, 'name' => 'Bearded Dragon'],
        ['parent_id' => $lizardId, 'name' => 'Iguana'],
        ['parent_id' => $lizardId, 'name' => 'Monitor'],
        ['parent_id' => $lizardId, 'name' => 'Anole'],
        ['parent_id' => $lizardId, 'name' => 'Chameleon'],
        ['parent_id' => $lizardId, 'name' => 'Water Dragon']
      ]);

      //Snake
      $snakeId = DB::table('pet_types')->where('name', 'Snake')
        ->value('id');
      DB::table('pet_types')->insert([
        ['parent_id' => $snakeId, 'name' => 'Corn Snake'],
        ['parent_id' => $snakeId, 'name' => 'California Kingsnake'],
        ['parent_id' => $snakeId, 'name' => 'Rosy Boa'],
        ['parent_id' => $snakeId, 'name' => 'Gopher Snake'],
        ['parent_id' => $snakeId, 'name' => 'Ball Python'],
        ['parent_id' => $snakeId, 'name' => 'King Snake'],
        ['parent_id' => $snakeId, 'name' => 'Milk Snake'],
        ['parent_id' => $snakeId, 'name' => 'Boa Constrictor'],
        ['parent_id' => $snakeId, 'name' => 'Burmese Python'],
        ['parent_id' => $snakeId, 'name' => 'Tree Boa'],
        ['parent_id' => $snakeId, 'name' => 'Water Snake'],
        ['parent_id' => $snakeId, 'name' => 'Green Snake'],
        ['parent_id' => $snakeId, 'name' => 'Anaconda'],
        ['parent_id' => $snakeId, 'name' => 'Red-Tail Boa'],
        ['parent_id' => $snakeId, 'name' => 'Black Rat Snake'],
        ['parent_id' => $snakeId, 'name' => 'Rattlesnake']
      ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $macawId = DB::table('pet_types')->where('name', 'Macaw')
          ->value('id');
        $higherIds = DB::table('pet_types')->where('id', '>', $macawId)
          ->pluck('id');
        foreach ($higherIds->reverse() as $idToDelete) {
          DB::table('pet_types')->where('id', $idToDelete)->delete();
        }
    }
}
