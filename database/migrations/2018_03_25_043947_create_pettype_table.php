<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePettypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //Create table
      Schema::create('pet_types', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('parent_id')->nullable();
          $table->string('name');
          $table->timestamps();

          $table->foreign('parent_id')->references('id')->on('pet_types');
      });

      //Insert pet types
      //Dogs
      $dogId = DB::table('pet_types')->insertGetId(
        ['name' => 'Dog']
      );
      DB::table('pet_types')->insert([
        ['parent_id' => $dogId, 'name' => 'Labrador Retriever'],
        ['parent_id' => $dogId, 'name' => 'Cocker Spaniel'],
        ['parent_id' => $dogId, 'name' => 'Springer Spaniel'],
        ['parent_id' => $dogId, 'name' => 'German Shepherd'],
        ['parent_id' => $dogId, 'name' => 'Staffordshire Bull Terrier'],
        ['parent_id' => $dogId, 'name' => 'Cavalier King Charles Spaniel'],
        ['parent_id' => $dogId, 'name' => 'Golden Retriever'],
        ['parent_id' => $dogId, 'name' => 'West Highland White Terrier'],
        ['parent_id' => $dogId, 'name' => 'Yorkshire Terrier'],
        ['parent_id' => $dogId, 'name' => 'Boxer'],
        ['parent_id' => $dogId, 'name' => 'Beagle'],
        ['parent_id' => $dogId, 'name' => 'Dachshund'],
        ['parent_id' => $dogId, 'name' => 'Poodle'],
        ['parent_id' => $dogId, 'name' => 'Miniature Schnauzer'],
        ['parent_id' => $dogId, 'name' => 'Shih Tzu'],
        ['parent_id' => $dogId, 'name' => 'Chihuahua'],
        ['parent_id' => $dogId, 'name' => 'Bulldog'],
        ['parent_id' => $dogId, 'name' => 'Pug'],
        ['parent_id' => $dogId, 'name' => 'Dalmation'],
        ['parent_id' => $dogId, 'name' => 'Great Dane']
      ]);

      //Cats
      $catId = DB::table('pet_types')->insertGetId(
        ['name' => 'Cat']
      );
      DB::table('pet_types')->insert([
        ['parent_id' => $catId, 'name' => 'Maine Coon'],
        ['parent_id' => $catId, 'name' => 'Ragamuffin'],
        ['parent_id' => $catId, 'name' => 'Ragdoll'],
        ['parent_id' => $catId, 'name' => 'Persian'],
        ['parent_id' => $catId, 'name' => 'American Shorthair'],
        ['parent_id' => $catId, 'name' => 'Abyssinian'],
        ['parent_id' => $catId, 'name' => 'Siamese'],
        ['parent_id' => $catId, 'name' => 'Tonkinese'],
        ['parent_id' => $catId, 'name' => 'Russian Blue'],
        ['parent_id' => $catId, 'name' => 'Bengal'],
        ['parent_id' => $catId, 'name' => 'Scottish Fold'],
        ['parent_id' => $catId, 'name' => 'British Shorthair'],
        ['parent_id' => $catId, 'name' => 'Siberian'],
        ['parent_id' => $catId, 'name' => 'Exotic'],
        ['parent_id' => $catId, 'name' => 'Birman'],
        ['parent_id' => $catId, 'name' => 'Himalayan'],
        ['parent_id' => $catId, 'name' => 'Nebelung'],
        ['parent_id' => $catId, 'name' => 'Norwegian Forest'],
        ['parent_id' => $catId, 'name' => 'American Bobtail'],
        ['parent_id' => $catId, 'name' => 'Bombay'],
        ['parent_id' => $catId, 'name' => 'Burmese'],
        ['parent_id' => $catId, 'name' => 'Chartreux'],
        ['parent_id' => $catId, 'name' => 'Chinese Li Hua'],
        ['parent_id' => $catId, 'name' => 'Devon Rex'],
        ['parent_id' => $catId, 'name' => 'Ocicat'],
        ['parent_id' => $catId, 'name' => 'Tabby']
      ]);

      //Birds
      $birdId = DB::table('pet_types')->insertGetId(
        ['name' => 'Bird']
      );

      //Parrots
      $parrotId = DB::table('pet_types')->insertGetId(
        ['parent_id' => $birdId, 'name' => 'Parrot']
      );
      $smallParrotId = DB::table('pet_types')->insertGetId(
        ['parent_id' => $parrotId, 'name' => 'Small Parrot']
      );
      DB::table('pet_types')->insert([
        ['parent_id' => $smallParrotId, 'name' => 'Cockatiel'],
        ['parent_id' => $smallParrotId, 'name' => 'Lovebird'],
        ['parent_id' => $smallParrotId, 'name' => 'Small Parakeet'],
        ['parent_id' => $smallParrotId, 'name' => 'Parrotlet']
      ]);
      $mediumParrotId = DB::table('pet_types')->insertGetId(
        ['parent_id' => $parrotId, 'name' => 'Medium Parrot']
      );
      DB::table('pet_types')->insert([
        ['parent_id' => $mediumParrotId, 'name' => 'Caiques'],
        ['parent_id' => $mediumParrotId, 'name' => 'Small Conure'],
        ['parent_id' => $mediumParrotId, 'name' => 'Lorikeet'],
        ['parent_id' => $mediumParrotId, 'name' => 'Large Parakeet'],
        ['parent_id' => $mediumParrotId, 'name' => 'Pionus Parrot'],
        ['parent_id' => $mediumParrotId, 'name' => 'Poicephalus']
      ]);
      $largeParrotId = DB::table('pet_types')->insertGetId(
        ['parent_id' => $parrotId, 'name' => 'Large Parrot']
      );
      DB::table('pet_types')->insert([
        ['parent_id' => $largeParrotId, 'name' => 'African Grey'],
        ['parent_id' => $largeParrotId, 'name' => 'Amazon'],
        ['parent_id' => $largeParrotId, 'name' => 'Cockatoo'],
        ['parent_id' => $largeParrotId, 'name' => 'Large Conure'],
        ['parent_id' => $largeParrotId, 'name' => 'Eclectus'],
        ['parent_id' => $largeParrotId, 'name' => 'Hawk Headed Parrot'],
        ['parent_id' => $largeParrotId, 'name' => 'Macaw'],
        ['parent_id' => $largeParrotId, 'name' => 'African Grey'],
      ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pet_types');
    }
}
