<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Log;

class DeleteDuplicatePetType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //We have a duplicate African grey!
      $africanGreyId = DB::table('pet_types')->where('name', 'African Grey')
        ->orderby('id', 'desc')
        ->first()->id;
      DB::table('pets')->where('pet_type_id', $africanGreyId)->delete();
      DB::table('article_pet_type')->where('pet_type_id', $africanGreyId)->delete();
      DB::table('pet_types')->where('id',$africanGreyId)->delete();


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      $largeParrotId = DB::table('pet_types')->where('name', 'Large Parrot')
        ->first()->id;
      DB::table('pet_types')->insert([
        ['parent_id' => $largeParrotId, 'name' => 'African Grey'],
      ]);
    }
}
