<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('content');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('article_pet_type', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('article_id');
          $table->unsignedInteger('pet_type_id');
          $table->timestamps();

          $table->foreign('article_id')->references('id')->on('articles')
                ->onDelete('cascade');
          $table->foreign('pet_type_id')->references('id')->on('pet_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_pet_type');
        Schema::dropIfExists('articles');
    }
}
