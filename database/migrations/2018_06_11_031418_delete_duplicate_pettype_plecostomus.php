<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Log;

class DeleteDuplicatePetTypePlecostomus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //We have a duplicate Plecostomus!
      $plecostomusId = DB::table('pet_types')->where('name', 'Plecostomus')
        ->orderby('id', 'desc')
        ->first()->id;
      DB::table('pets')->where('pet_type_id', $plecostomusId)->delete();
      DB::table('article_pet_type')->where('pet_type_id', $plecostomusId)->delete();
      DB::table('pet_types')->where('id',$plecostomusId)->delete();


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      $fishId = DB::table('pet_types')->where('name', 'Fish')
        ->first()->id;
      DB::table('pet_types')->insert([
        ['parent_id' => $fishId, 'name' => 'Plecostomus'],
      ]);
    }
}
