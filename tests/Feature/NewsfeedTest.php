<?php

namespace Tests\Feature;

use App\Article;
use App\User;
use App\PetType;
use App\Pet;
use Tests\TestCase;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NewsfeedTest extends TestCase
{
  use RefreshDatabase;

    /**
     * If there are 10 cat articles and 10 dog articles with alternating dates
     * and the user is not signed in, getting the newsfeed gets the 10 latest articles.
     */
    public function testNotSignedIn_Gets10LatestArticles()
    {
      //create 10 articles with pet type 'cat'
      $catArticles = factory(Article::class, 10)
        ->create()
        ->each(function ($u) {
          $u->petTypes()->attach(PetType::where('name', 'Cat')->first()->id);
        });
      //create 10 articles with pet type 'dog'
      $dogArticles = factory(Article::class, 10)
        ->create()
        ->each(function ($u) {
          $u->petTypes()->attach(PetType::where('name', 'Dog')->first()->id);
        });

      //Make them created in the last 20 days, one per day, alternating between
      //cat and dog.
      for ($i = 0; $i < 20; $i++) {
        if ($i % 2 == 0) {
          $catArticles[intdiv($i, 2)]->created_at = Carbon::now()->subDays($i);
          $catArticles[intdiv($i, 2)]->save();
        }
        else {
          $dogArticles[intdiv($i, 2)]->created_at = Carbon::now()->subDays($i);
          $dogArticles[intdiv($i, 2)]->save();
        }
      }

      $response = $this->get('/newsfeed');
      $response->assertStatus(200);
      $articlesReturned = $response->baseResponse->original;
      $this->assertCount(10, $articlesReturned);

      //Assert the first two manually to make sure we got our maths right...
      $this->assertEquals($catArticles[0]->id, $articlesReturned[0]->id);
      $this->assertEquals(Carbon::now(), $articlesReturned[0]->created_at, '', 1);

      $this->assertEquals($dogArticles[0]->id, $articlesReturned[1]->id);
      $this->assertEquals(Carbon::now()->subDays(1), $articlesReturned[1]->created_at, '', 1);

      for($i = 2; $i < 10; $i++) {
        if ($i % 2 == 0) {
          $this->assertEquals($catArticles[intdiv($i,2)]->id, $articlesReturned[$i]->id);
        }
        else {
          $this->assertEquals($dogArticles[intdiv($i,2)]->id, $articlesReturned[$i]->id);
        }

        $this->assertEquals(Carbon::now()->subDays($i), $articlesReturned[$i]->created_at, '', 1);
      }
    }

    /**
     * If there are 10 cat articles and 10 dog articles with alternating dates
     * and the user has no pets, getting the newsfeed gets the 10 latest articles.
     */
    public function testNoPets_Gets10LatestArticles()
    {
      $user = factory(User::class)->create();

      //create 10 articles with pet type 'cat'
      $catArticles = factory(Article::class, 10)
        ->create()
        ->each(function ($u) {
          $u->petTypes()->attach(PetType::where('name', 'Cat')->first()->id);
        });
      //create 10 articles with pet type 'dog'
      $dogArticles = factory(Article::class, 10)
        ->create()
        ->each(function ($u) {
          $u->petTypes()->attach(PetType::where('name', 'Dog')->first()->id);
        });

      //Make them created in the last 20 days, one per day, alternating between
      //cat and dog.
      for ($i = 0; $i < 20; $i++) {
        if ($i % 2 == 0) {
          $catArticles[intdiv($i, 2)]->created_at = Carbon::now()->subDays($i);
          $catArticles[intdiv($i, 2)]->save();
        }
        else {
          $dogArticles[intdiv($i, 2)]->created_at = Carbon::now()->subDays($i);
          $dogArticles[intdiv($i, 2)]->save();
        }
      }

      $response = $this->actingAs($user)->get('/newsfeed');
      $response->assertStatus(200);
      $articlesReturned = $response->baseResponse->original;
      $this->assertCount(10, $articlesReturned);

      //Assert the first two manually to make sure we got our maths right...
      $this->assertEquals($catArticles[0]->id, $articlesReturned[0]->id);
      $this->assertEquals(Carbon::now(), $articlesReturned[0]->created_at, '', 1);

      $this->assertEquals($dogArticles[0]->id, $articlesReturned[1]->id);
      $this->assertEquals(Carbon::now()->subDays(1), $articlesReturned[1]->created_at, '', 1);

      for($i = 2; $i < 10; $i++) {
        if ($i % 2 == 0) {
          $this->assertEquals($catArticles[intdiv($i,2)]->id, $articlesReturned[$i]->id);
        }
        else {
          $this->assertEquals($dogArticles[intdiv($i,2)]->id, $articlesReturned[$i]->id);
        }
        $this->assertEquals(Carbon::now()->subDays($i), $articlesReturned[$i]->created_at, '', 1);
      }
    }

    /**
     * If there are 10 cat articles and 10 dog articles with alternating dates
     * and the user has a pet bird, getting the newsfeed gets the 10 latest articles.
     */
    public function testIrrelevantPets_Gets10LatestArticles()
    {
      $user = factory(User::class)->create();
      $user->pets()->save(factory(Pet::class)->states('smallParrot')->make());

      //create 10 articles with pet type 'cat'
      $catArticles = factory(Article::class, 10)
        ->create()
        ->each(function ($u) {
          $u->petTypes()->attach(PetType::where('name', 'Cat')->first()->id);
        });
      //create 10 articles with pet type 'dog'
      $dogArticles = factory(Article::class, 10)
        ->create()
        ->each(function ($u) {
          $u->petTypes()->attach(PetType::where('name', 'Dog')->first()->id);
        });

      //Make them created in the last 20 days, one per day, alternating between
      //cat and dog.
      for ($i = 0; $i < 20; $i++) {
        if ($i % 2 == 0) {
          $catArticles[intdiv($i, 2)]->created_at = Carbon::now()->subDays($i);
          $catArticles[intdiv($i, 2)]->save();
        }
        else {
          $dogArticles[intdiv($i, 2)]->created_at = Carbon::now()->subDays($i);
          $dogArticles[intdiv($i, 2)]->save();
        }
      }

      $response = $this->actingAs($user)->get('/newsfeed');
      $response->assertStatus(200);
      $articlesReturned = $response->baseResponse->original;
      $this->assertCount(10, $articlesReturned);

      //Assert the first two manually to make sure we got our maths right...
      $this->assertEquals($catArticles[0]->id, $articlesReturned[0]->id);
      $this->assertEquals(Carbon::now(), $articlesReturned[0]->created_at, '', 1);

      $this->assertEquals($dogArticles[0]->id, $articlesReturned[1]->id);
      $this->assertEquals(Carbon::now()->subDays(1), $articlesReturned[1]->created_at, '', 1);

      for($i = 2; $i < 10; $i++) {
        if ($i % 2 == 0) {
          $this->assertEquals($catArticles[intdiv($i,2)]->id, $articlesReturned[$i]->id);
        }
        else {
          $this->assertEquals($dogArticles[intdiv($i,2)]->id, $articlesReturned[$i]->id);
        }
        $this->assertEquals(Carbon::now()->subDays($i), $articlesReturned[$i]->created_at, '', 1);
      }
    }

    public function testNewsfeedPrefersExactMatchingPets()
    {
      //User has small parrot
      $user = factory(User::class)->create();
      $user->pets()->save(factory(Pet::class)->states('smallParrot')->make());

      //10 articles today about small parrots
      $smallParrotArticles = factory(Article::class, 10)
        ->create()
        ->each(function ($u) {
          $u->petTypes()->attach(PetType::where('name', 'Small Parrot')->first()->id);
        });

      //10 articles today cockatiel (descendant)
      $cockatielArticles = factory(Article::class, 10)
        ->create()
        ->each(function ($u) {
          $u->petTypes()->attach(PetType::where('name', 'Cockatiel')->first()->id);
        });

      //10 articles today parrots (parent)
      $parrotArticles = factory(Article::class, 10)
        ->create()
        ->each(function ($u) {
          $u->petTypes()->attach(PetType::where('name', 'Parrot')->first()->id);
        });

      //10 articles today about dogs (unrelated)
      $dogArticles = factory(Article::class, 10)
        ->create()
        ->each(function ($u) {
          $u->petTypes()->attach(PetType::where('name', 'Dog')->first()->id);
        });

      $response = $this->actingAs($user)->get('/newsfeed');
      $response->assertStatus(200);
      $articlesReturned = $response->baseResponse->original;
      $this->assertCount(10, $articlesReturned);

      $smallParrotPetTypeId = PetType::where('name', 'Small Parrot')->first()->id;
      foreach($articlesReturned as $articleReturned) {
        $article = Article::find($articleReturned->id);
        $this->assertTrue($article->petTypes->contains('id', $smallParrotPetTypeId));
      }
    }

    public function testNewsfeedPrefersParentPetsToChildren()
    {
      //User has small parrot
      $user = factory(User::class)->create();
      $user->pets()->save(factory(Pet::class)->states('smallParrot')->make());

      //10 articles today cockatiel (descendant)
      $cockatielArticles = factory(Article::class, 10)
        ->create()
        ->each(function ($u) {
          $u->petTypes()->attach(PetType::where('name', 'Cockatiel')->first()->id);
        });

      //10 articles today parrots (parent)
      $parrotArticles = factory(Article::class, 10)
        ->create()
        ->each(function ($u) {
          $u->petTypes()->attach(PetType::where('name', 'Parrot')->first()->id);
        });

      $response = $this->actingAs($user)->get('/newsfeed');
      $response->assertStatus(200);
      $articlesReturned = $response->baseResponse->original;
      $this->assertCount(10, $articlesReturned);

      $parrotPetTypeId = PetType::where('name', 'Parrot')->first()->id;
      foreach($articlesReturned as $articleReturned) {
        $article = Article::find($articleReturned->id);
        $this->assertTrue($article->petTypes->contains('id', $parrotPetTypeId));
      }
    }

    public function testNewsfeedPrefersChildPetsToUnrelated()
    {
      //User has small parrot
      $user = factory(User::class)->create();
      $user->pets()->save(factory(Pet::class)->states('smallParrot')->make());

      //10 articles today cockatiel (descendant)
      $cockatielArticles = factory(Article::class, 10)
        ->create()
        ->each(function ($u) {
          $u->petTypes()->attach(PetType::where('name', 'Cockatiel')->first()->id);
        });

      //10 articles today about dogs (unrelated)
      $dogArticles = factory(Article::class, 10)
        ->create()
        ->each(function ($u) {
          $u->petTypes()->attach(PetType::where('name', 'Dog')->first()->id);
        });

      $response = $this->actingAs($user)->get('/newsfeed');
      $response->assertStatus(200);
      $articlesReturned = $response->baseResponse->original;
      $this->assertCount(10, $articlesReturned);

      $cockatielPetTypeId = PetType::where('name', 'Cockatiel')->first()->id;
      foreach($articlesReturned as $articleReturned) {
        $article = Article::find($articleReturned->id);
        $this->assertTrue($article->petTypes->contains('id', $cockatielPetTypeId));
      }
    }

    public function testNewsfeed_ReturnsArticlesAccordingToPetWeightings()
    {
      //User has small parrot
      $user = factory(User::class)->create();
      $user->pets()->save(factory(Pet::class)->states('smallParrot')->make());

      //Create 9 Articles - weighting shown as comment
      $dog1 = $this->makeArticleWithPetTypeAndDaysOld('Dog', 0); //1
      $dog2 = $this->makeArticleWithPetTypeAndDaysOld('Dog', 2); //0.33
      $cockatiel1 = $this->makeArticleWithPetTypeAndDaysOld('Cockatiel', 3); //0.5
      $parrot1 = $this->makeArticleWithPetTypeAndDaysOld('Parrot', 5); //0.83
      $dog3 = $this->makeArticleWithPetTypeAndDaysOld('Dog', 6); //0.14
      $smallParrot1 = $this->makeArticleWithPetTypeAndDaysOld('Small Parrot', 7); //1.25
      $cockatiel2 = $this->makeArticleWithPetTypeAndDaysOld('Cockatiel', 8); //0.22
      $dog4 = $this->makeArticleWithPetTypeAndDaysOld('Dog', 9); //0.1
      $smallParrot2 = $this->makeArticleWithPetTypeAndDaysOld('Small Parrot', 10); //0.91

      $response = $this->actingAs($user)->get('/newsfeed');
      $response->assertStatus(200);
      //Log::debug(json_encode($response->baseResponse->original));
      $articlesReturned = $response->baseResponse->original;
      $this->assertCount(9, $articlesReturned);

      //Should be correctly sorted by weighting
      $this->assertEquals($smallParrot1->id, $articlesReturned[0]->id);
      $this->assertEquals($dog1->id, $articlesReturned[1]->id);
      $this->assertEquals($smallParrot2->id, $articlesReturned[2]->id);
      $this->assertEquals($parrot1->id, $articlesReturned[3]->id);
      $this->assertEquals($cockatiel1->id, $articlesReturned[4]->id);
      $this->assertEquals($dog2->id, $articlesReturned[5]->id);
      $this->assertEquals($cockatiel2->id, $articlesReturned[6]->id);
      $this->assertEquals($dog3->id, $articlesReturned[7]->id);
      $this->assertEquals($dog4->id, $articlesReturned[8]->id);
    }

    private function makeArticleWithPetTypeAndDaysOld($petType, $daysOld)
    {
      $article = factory(Article::class)->create();
      $article->petTypes()->attach(PetType::where('name', $petType)->first()->id);
      $article->created_at = Carbon::now()->subDays($daysOld);
      $article->save();
      return $article;
    }
}
