<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'ArticleController@newsfeed')->name('home');
Route::get('/spa', 'ArticleController@spa');

//While this returns JSON it isn't a proper Rest api
//because it isn't stateless and relies on the user's Session
//to customise the results according to the logged-in user.
Route::get('/newsfeed', 'ArticleController@newsfeedJson');

Route::post('articles/storeComment', 'ArticleController@storeComment');

Route::resource('articles', 'ArticleController');

Route::resource('pets', 'PetController', [
   'except' => ['show']
 ]);

 Route::get('/users/preferences', 'UserController@edit');
 Route::put('/users/updatepreferences', 'UserController@update');

Route::resource('users', 'UserController', [
  'only' => ['edit', 'update']
]);

Route::get('code', function(){
  return Redirect::to('https://bitbucket.org/danielelkington/pethelp/src/master/');
});

//Uncomment this to test how emails display!
// Route::get('/mailtest', function() {
//   $article = App\Article::find(4);
//   $user = App\User::find(1);
//   return new App\Mail\NewArticle($article, $user);
// });
