@extends('layouts.app')

@section('header')
My Articles
@endsection

@section('content')
@if (count($articles) > 0)
<a href="{{URL::to('articles/create/')}}">Create an article</a>
<table class="htable table-striped table-bordered">
  <thead>
    <tr>
      <th>Title</th>
      <th>Content</th>
      <th>Creation date/time (UTC time)</th>
    </tr>
  </thead>
  <tbody>
    @foreach($articles as $key => $value)
      <tr>
        <td>{{ $value->title }}</td>
        <td>{{ $value->content_preview }}</td>
        <td>{{ $value->created_at}}</td>
        <td>
          <!--Edit-->
          <a class="btn btn-small btn-info" href="{{URL::to('articles/' . $value->id . '/edit')}}">Edit</a>
        </td>
        <td>
          <form method="POST" action="{{url('articles/' . $value->id)}}" class="pull-right">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE"></input>
            <input class="btn btn-danger" type="submit" value="Delete"></input>
          </form>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
@else
<p>You have no articles!<p>
@endif
<a href="{{URL::to('articles/create/')}}">Create an article</a>
@endsection
