@extends('layouts.app')

@section('header')
Edit Article
@endsection

@section('content')
<form method="POST" action="{{url('articles/' . $article->id)}}">
  {{csrf_field()}}
  <input name="_method" type="hidden" value="PUT"></input>
  <label for="title">Title:</label>
  <input type="text" name="title" id="title" class="form-control"
    value="{{old('title', $article->title)}}"></input>
  <br/>
  <span>Relevant Pet Types:</span>
  <pet-type-selector :initial-pet-types="{{json_encode($petTypes)}}"></pet-type-selector>
  <textarea id="article-editor" name="content" placeholder="Type your article here..."></textarea>
  <input class="btn btn-primary" type="submit" value="Update article"></input>
</form>
<textarea id="old-article-content" style="display: none;">{{old('content', $article->content)}}</textarea>
@endsection
