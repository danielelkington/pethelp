@extends('layouts.app')

@section('header')
Create a new Article
@endsection

@section('content')
<form method="POST" action="{{url('articles')}}">
  {{csrf_field()}}
  <label for="title">Title:</label>
  <input type="text" name="title" id="title" class="form-control"
    value="{{old('title')}}"></input>
  <br/>
  <span>Relevant Pet Types:</span>
  <pet-type-selector :initial-pet-types="{{json_encode($petTypes)}}"></pet-type-selector>
  <textarea id="article-editor" name="content" placeholder="Type your article here..."></textarea>
  <input class="btn btn-primary" type="submit" value="Create article"></input>
</form>
<textarea id="old-article-content" style="display: none;">{{old('content')}}</textarea>
@endsection
