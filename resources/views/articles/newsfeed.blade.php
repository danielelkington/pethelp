@extends('layouts.app')

@section('content')
@guest
<p>You aren't signed in! <a href="{{ route('login') }}">{{ __('Login') }}</a> now to see articles relevant to your pets.</p>
@elseif (Auth::user()->pets()->count() === 0)
<p>You have no pets! <a href="{{URL::to('pets/create')}}">Add a pet now</a> now to see articles relevant to your pets.</p>
@endauth

@foreach($articles as $article)
<h2 class="nomargin"><a href="{{ URL::to('articles/' . $article->id)}}">{{$article->title}}</a></h2>
<small>By {{$article->user->name}} - {{$article->comments_count}} comment{{$article->comments_count == 1 ? '' : 's'}}</small>
<br/><small>{{$article->days_old_description}}</small>
<div class="gap"></div>
{!!$article->content_preview_with_tags!!}
<a href="{{ URL::to('articles/' . $article->id)}}">Read full article</a>
<hr/>
<br/>
@endforeach

@endsection
