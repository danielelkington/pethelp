@extends('layouts.app')

@section('content')
@guest
<small>You aren't signed in! <a href="{{ route('login') }}">{{ __('Login') }}</a> now to see articles relevant to your pets.</small>
@elseif (Auth::user()->pets()->count() === 0)
<small>You have no pets! <a href="{{URL::to('pets/create')}}">Add a pet now</a> now to see articles relevant to your pets.</small>
@endguest

<h1 class="nomargin">{{$article->title}}</h1>
<small>By {{$article->user->name}}</small>
<br/><small>{{$article->days_old_description}}</small>
<div class="gap"></div>
{!!$article->content!!}
<hr/>
<h3>Comments</h3>
@if($article->comments()->count() === 0)
<p>This article has no comments at the moment.</p>
@endif
@foreach($article->comments as $comment)
<p>{{$comment->content}}</p>
<small>By {{$comment->user->name}}</small>
<hr/>
@endforeach
@guest
<a href="{{ route('login') }}">{{ __('Login') }}</a> to add a comment.
@else
<form method="POST" action="{{url('articles/storeComment')}}">
  {{csrf_field()}}
  <textarea name="content" id="content" class="form-control" placeholder="Your comment here...">
    {{old('content')}}</textarea>
  <br/>
  <input type="hidden" name="article_id" id= "article_id" value="{{$article->id}}"></input>
  <input class="btn btn-primary" type="submit" value="Add Comment"></input>
</form>
@endguest
@endsection
