<tr>
    <td class="article-header">
        <a href="{{ $url }}">
            {{ $slot }}
        </a>
    </td>
</tr>
