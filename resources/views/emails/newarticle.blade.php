@component('mail::message')
{{$user->name}} - A new pethelp.online article has been published to help you and your pets!

@component('mail::articleheader', ['url' => URL::to('articles/' . $article->id)])
{{$article->title}}
@endcomponent

@component('mail::panel')
{!!$article->content_preview_with_tags!!}
@endcomponent

@component('mail::button', ['url' => URL::to('articles/' . $article->id)])
Read the full article
@endcomponent

@component('mail::footer')
You're receiving these emails because you subscribed on <a href="https://pethelp.online">pethelp.online</a>.
<br/>To manage your preferences, click <a href="{{URL::to('users/preferences')}}">here</a>.
@endcomponent
@endcomponent
