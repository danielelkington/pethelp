@extends('layouts.app')

@section('header')
My Pets
@endsection

@section('content')
@foreach($pets as $key => $value)
  <h2>{{$value->name}}</h2>
  <span class="description">
  @if ($value->petType->parent_id != NULL)
    {{$value->petType->parent->name}} - {{$value->petType->name}}
  @else
    {{$value->petType->name}}
  @endif
  </span>
  <a class="btn btn-info" href="{{URL::to('pets/' . $value->id . '/edit')}}">Edit</a>
  <!--Delete-->
  <form method="POST" action="{{url('pets/' . $value->id)}}" class="inline-form">
    {{csrf_field()}}
    <input name="_method" type="hidden" value="DELETE"></input>
    <input class="btn btn-danger" type="submit" value="Delete"></input>
  </form>
  <br/>
  <br/>
@endforeach
@if (count($pets) === 0)
  <p>You have no pets!<p>
@endif
<a class="btn btn-primary btn-small" role="button" href="{{URL::to('pets/create')}}">Add Pet</a>
@endsection
