@extends('layouts.app')

@section('header')
Add a Pet you own
@endsection

@section('content')
<form method="POST" action="{{url('pets')}}">
  {{csrf_field()}}
  <label for="name">Name:</label>
  <input type="text" name="name" id="name" class="form-control"
        value="{{old('name')}}"></input>
  <br/>
  <span>Pet Type (select 1 - the more specific the better):</span>
  <pet-type-selector :initial-pet-types="{{json_encode($petTypes)}}" :only-select-one="true"></pet-type-selector>
  <input class="btn btn-primary" type="submit" value="Done"></input>
</form>
@endsection
