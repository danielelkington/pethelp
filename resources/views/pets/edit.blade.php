@extends('layouts.app')

@section('header')
Edit pet
@endsection

@section('content')
<form method="POST" action="{{url('pets/' . $pet->id)}}">
  {{csrf_field()}}
  <input name="_method" type="hidden" value="PUT"></input>
  <label for="name">Name:</label>
  <input type="text" name="name" id="name" value="{{old('name', $pet->name)}}" class="form-control"></input>
  <br/>
  <span>Pet Type (select 1 - the more specific the better):</span>
  <pet-type-selector :initial-pet-types="{{json_encode($petTypes)}}" :only-select-one="true"></pet-type-selector>
  <input class="btn btn-primary" type="submit" value="Done"></input>
</form>
@endsection
