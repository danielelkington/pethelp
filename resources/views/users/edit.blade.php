@extends('layouts.app')

@section('header')
Edit preferences
@endsection

@section('content')
<form method="POST" action="{{url('users/updatepreferences')}}">
  {{csrf_field()}}
  <input name="_method" type="hidden" value="PUT"></input>
  <div class="form-check">
    <input type="checkbox" name="is_subscribed"
      id="is_subscribed"
      value="1" 
      class="form-check-input"
      @if(old('is_subscribed', $user->is_subscribed) === 1) checked @endif></input>
    <label for="is_subscribed" class="form-check-label">Subscribe to email updates</label>
  </div>
  <input class="btn btn-primary" type="submit" value="Update"></input>
</form>
@endsection
