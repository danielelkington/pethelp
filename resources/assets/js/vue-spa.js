import VueRouter from 'vue-router';

if (window.location.href.indexOf('/spa') !== -1){
  Vue.use(VueRouter);


  //const Newsfeed = { template: '<div>The Newsfeed!</div>'};
  const Article = { template: '<div>An article! {{ $route.params.id }}</div>'};

  const routes = [
    { path: '/', component: require('./components/NewsfeedComponent.vue')},
    { path: '/articles/:id', component: require('./components/ArticleComponent.vue')}
  ];

  const router = new VueRouter({
    routes
  });

  const spaApp = new Vue({
    router
  }).$mount('#spa');
}
