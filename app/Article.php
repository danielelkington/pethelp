<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Article extends Model
{
    public function petTypes() {
      return $this->belongsToMany('App\PetType');
    }

    public function comments() {
      return $this->hasMany('App\Comment');
    }

    public function user() {
      return $this->belongsTo('App\User');
    }

    protected $appends = array('content_preview', 'content_preview_with_tags', 'days_old_description');

    public function getContentPreviewAttribute() {
      $preview = strip_tags($this->content);
      if (strlen($preview) < 50) {
        return $preview;
      }
      return substr($preview, 0, 50) . '...';
    }

    public function getContentPreviewWithTagsAttribute() {
      $preview = $this->html_cut($this->content, 200);
      return $preview;
    }

    public function getDaysOldDescriptionAttribute() {
      $daysOld = $this->created_at->diffInDays(Carbon::now());
      if ($daysOld === 0) {
        return "Today";
      }
      if ($daysOld === 1) {
        return "Yesterday";
      }
      return $daysOld . " days old";
    }

    //Neat little function from https://stackoverflow.com/a/2398820/5740181
    //Cuts a HTML string to specified length without breaking HTLM tags
    function html_cut($text, $max_length)
    {
        $tags   = array();
        $result = "";

        $is_open   = false;
        $grab_open = false;
        $is_close  = false;
        $in_double_quotes = false;
        $in_single_quotes = false;
        $tag = "";

        $i = 0;
        $stripped = 0;

        $stripped_text = strip_tags($text);

        while ($i < strlen($text) && $stripped < strlen($stripped_text) && $stripped < $max_length)
        {
            $symbol  = $text{$i};
            $result .= $symbol;

            switch ($symbol)
            {
               case '<':
                    $is_open   = true;
                    $grab_open = true;
                    break;

               case '"':
                   if ($in_double_quotes)
                       $in_double_quotes = false;
                   else
                       $in_double_quotes = true;

                break;

                case "'":
                  if ($in_single_quotes)
                      $in_single_quotes = false;
                  else
                      $in_single_quotes = true;

                break;

                case '/':
                    if ($is_open && !$in_double_quotes && !$in_single_quotes)
                    {
                        $is_close  = true;
                        $is_open   = false;
                        $grab_open = false;
                    }

                    break;

                case ' ':
                    if ($is_open)
                        $grab_open = false;
                    else
                        $stripped++;

                    break;

                case '>':
                    if ($is_open)
                    {
                        $is_open   = false;
                        $grab_open = false;
                        array_push($tags, $tag);
                        $tag = "";
                    }
                    else if ($is_close)
                    {
                        $is_close = false;
                        array_pop($tags);
                        $tag = "";
                    }

                    break;

                default:
                    if ($grab_open || $is_close)
                        $tag .= $symbol;

                    if (!$is_open && !$is_close)
                        $stripped++;
            }

            $i++;
        }

        while ($tags)
            $result .= "</".array_pop($tags).">";

        return $result;
    }
}
