<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetType extends Model
{
  public function articles() {
    return $this->belongsToMany('App\Article');
  }

  public function pets() {
    return $this->hasMany('App\Pet');
  }

  public function parent() {
    return $this->belongsTo('App\PetType', 'parent_id');
  }

  public function childPetTypes() {
    return $this->hasMany('App\PetType', 'parent_id');
  }

  protected $appends = array('children', 'checked', 'open');
  public function getChildrenAttribute() {
    return $this->children;
  }
  public function getCheckedAttribute() {
    return $this->checked;
  }
  public function getOpenAttribute() {
    return $this->open;
  }
  public $children = array();
  public $checked = false;
  public $open = false;
}
