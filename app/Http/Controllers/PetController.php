<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Pet;
use App\PetType;
use App\Http\Requests\StorePet;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Session;

class PetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pets = Pet::where('user_id', Auth::id())->get();
        return View::make('pets.index')->with('pets', $pets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $petTypes = $this->getPetTypeHierarchy();
      return View::make('pets.create')->with('petTypes', $petTypes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePet $request)
    {
        $validated = $request->validated();
        $pet = new Pet;
        $pet->name = $request->input('name');
        $pet->pet_type_id = $request->input('pettype')[0];
        $pet->user_id = Auth::id();
        $pet->save();

        //Redirect
        Session::flash('message', 'Successfully added pet!');
        return Redirect::to('pets');
    }

    private function checkPetType($petType, $checkedPetTypeId, $parents)
    {
      if ($petType->id === $checkedPetTypeId) {
        $petType->checked = true;
        foreach ($parents as $parent) {
          $parent->open = true;
        }
      } else {
        $parents[] = $petType;
        foreach ($petType->children as $childPetType) {
          $parentsCopy = $parents;
          $this->checkPetType($childPetType, $checkedPetTypeId, $parentsCopy);
        }
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pet = Pet::find($id);
        $petTypes = $this->getPetTypeHierarchy();
        foreach ($petTypes as $petType) {
          $this->checkPetType($petType, $pet->pet_type_id, array($petType));
        }

        return View::make('pets.edit')
          ->with('pet', $pet)
          ->with('petTypes', $petTypes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePet $request, $id)
    {
        $validated = $request->validated();
        $pet = Pet::find($id);
        if ($pet->user_id != Auth::id()) {
          abort(403, 'Cannot edit pets you don\'t own!');
        }
        $pet->name = $request->input('name');
        $pet->pet_type_id = $request->input('pettype')[0];
        $pet->save();

        //Redirect
        Session::flash('message', 'Successfully updated pet!');
        return Redirect::to('pets');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pet = Pet::find($id);
        if ($pet->user_id != Auth::id()) {
          abort(403, 'User attempting to delete a pet that they do not own.');
        }
        $pet->delete();

        Session::flash('message', 'Successfully deleted this pet.');
        return Redirect::to('pets');
    }

    private function getPetTypeHierarchy()
    {
      $petTypes = PetType::all();
      $parentPets = array();
      //Process pet types into parent-child relationship
      foreach ($petTypes as $petType) {
        if ($petType->parent_id === NULL) {
          $parentPets[] = $petType;
        }
        else {
          foreach ($petTypes as $potentialParentPetType) {
            if ($petType->parent_id === $potentialParentPetType->id) {
              $potentialParentPetType->children[] = $petType;
              break;
            }
          }
        }
      }
      return $parentPets;
    }
}
