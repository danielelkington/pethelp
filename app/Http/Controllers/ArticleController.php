<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\PetType;
use App\Article;
use App\Comment;
use App\Http\Requests\StoreArticle;
use App\Http\Requests\StoreComment;
use App\Jobs\ProcessNewArticleEmails;
use Carbon\Carbon;
use Input;
use Redirect;
use Session;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => [
            'spa',
            'newsfeed',
            'newsfeedJson',
            'show',
            'articleJson']]);
    }

    //Return the newsfeed of articles!
    public function newsfeed(Request $request)
    {
      $articles = $this->getNewsfeedArticles($request);
      return View::make('articles.newsfeed')->with('articles', $articles);
    }

    public function newsfeedJson(Request $request)
    {
      $articles = $this->getNewsfeedArticles($request);
      //Limit unnecesary properties from being returned in the JSON.
      $articles->makeHidden(['content', 'content_preview', 'user_id']);
      foreach($articles as $article) {
        $article->user->makeHidden(['id', 'is_subscribed', 'email', 'created_at', 'updated_at']);
      }
      return response()->json($articles, 200);
    }

    public function articleJson($id)
    {
      $article = Article::with('user')->with('comments.user')->find($id);
      $article->makeHidden(['content_preview', 'user_id', 'content_preview_with_tags']);
      $article->user->makeHidden(['id', 'is_subscribed', 'email', 'created_at', 'updated_at']);
      foreach($article->comments as $comment) {
        $comment->user->makeHidden(['id', 'is_subscribed', 'email', 'created_at', 'updated_at']);
      }
      return response()->json($article, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::where('user_id', Auth::id())->get();
        return View::make('articles.index')->with('articles', $articles);
    }

    //Allows us to view a single page application version of the app.
    public function spa()
    {
      return View::make('layouts.spa');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $petTypes = $this->getPetTypeHierarchy();
      return View::make('articles.create')->with('petTypes', $petTypes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArticle $request)
    {
      $validated = $request->validated();

      Db::transaction(function() use ($request) {
        //1. Add the article
        $article = new Article;
        $article->title = $request->input('title');
        //Clean input to prevent XSS attacks!
        $article->content = clean($request->input('content'));
        $article->user_id = Auth::id();
        $article->save();
        //2. Add the pet Types
        $pettypeids = $request->input('pettype'); //[1, 2, 4]
        if ($pettypeids == NULL)
          $pettypeids = [];
        $article->petTypes()->sync($pettypeids);

        ProcessNewArticleEmails::dispatch($article)->onQueue('lowPriorityEmails');
      });

      //Redirect
      Session::flash('message', 'Successfully created article!');
      return Redirect::to('articles');
    }

    public function storeComment(StoreComment $request)
    {
        $validated = $request->validated();
        $comment = new Comment;
        $comment->content = $request->input('content');
        $comment->article_id = $request->input('article_id');
        $comment->user_id = Auth::id();
        $comment->save();

        //Redirect
        Session::flash('message', 'Successfully created comment!');
        return Redirect::to('articles/' . $comment->article_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $article = Article::with('comments')->with('user')->find($id);
      return View::make('articles.show')->with('article', $article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $petTypes = $this->getPetTypeHierarchy();
        $checkedPetTypeIds = $article->petTypes()
                        ->pluck('pet_types.id')->toArray();

        foreach ($checkedPetTypeIds as $checkedPetTypeId) {
          foreach ($petTypes as $petType) {
            $this->checkPetType($petType, $checkedPetTypeId, array($petType));
          }
        }

        return View::make('articles.edit')
          ->with('article', $article)
          ->with('petTypes', $petTypes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreArticle $request, $id)
    {
      $validated = $request->validated();

      $article = Article::find($id);
      if ($article->user_id != Auth::id()) {
        abort(403, 'Cannot edit articles you don\'t own!');
      }

      $article->title = $request->input('title');
      //Clean input to prevent XSS attacks!
      $article->content = clean($request->input('content'));
      $pettypeids = $request->input('pettype'); //[1, 2, 4]
      if ($pettypeids == NULL)
        $pettypeids = [];
      $article->petTypes()->sync($pettypeids);
      $article->save();

      //ProcessNewArticleEmails::dispatch($article)->onQueue('lowPriorityEmails');

      //Redirect
      Session::flash('message', 'Successfully updated article!');
      return Redirect::to('articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $article = Article::find($id);
      if ($article->user_id != Auth::id()) {
        abort(403, 'User attempting to delete an article that they do not own.');
      }
      $article->delete();

      Session::flash('message', 'Successfully deleted this article.');
      return Redirect::to('articles');
    }

    private function getPetTypeHierarchy()
    {
      $petTypes = PetType::all();
      $parentPets = array();
      //Process pet types into parent-child relationship
      foreach ($petTypes as $petType) {
        if ($petType->parent_id === NULL) {
          $parentPets[] = $petType;
        }
        else {
          foreach ($petTypes as $potentialParentPetType) {
            if ($petType->parent_id === $potentialParentPetType->id) {
              $potentialParentPetType->children[] = $petType;
              break;
            }
          }
        }
      }
      return $parentPets;
    }

    private function checkPetType($petType, $checkedPetTypeId, $parents)
    {
      if ($petType->id === $checkedPetTypeId) {
        $petType->checked = true;
        foreach ($parents as $parent) {
          $parent->open = true;
        }
      } else {
        $parents[] = $petType;
        foreach ($petType->children as $childPetType) {
          $parentsCopy = $parents;
          $this->checkPetType($childPetType, $checkedPetTypeId, $parentsCopy);
        }
      }
    }

    private function getUniqueParents($petType, & $foundParents, $ignore)
    {
      if ($petType->parent == NULL ||
        in_array($petType->parent_id, $foundParents))
      {
        return;
      }
      if (!in_array($petType->parent_id, $ignore))
      {
        $foundParents[] = $petType->parent_id;
      }
      $this->getUniqueParents($petType->parent, $foundParents, $ignore);
    }

    private function getUniqueChildren($petType, & $foundChildren, $ignore)
    {
      if (in_array($petType->id, $foundChildren))
      {
        return;
      }
      if (!in_array($petType->id, $ignore))
      {
        $foundChildren[] = $petType->id;
      }
      foreach($petType->childPetTypes as $childPetType)
      {
        $this->getUniqueChildren($childPetType, $foundChildren, $ignore);
      }
    }

    private function getNewsfeedArticles(Request $request)
    {
      //If not signed in, return top 20 articles by created date desc
      if (!Auth::check()) {
        return Article::latest()->with('user')->withCount('comments')->take(10)->get();
      }

      //Signed in. Get user's pet types.
      $userPetTypes = $request->user()->pets()->with('petType')->get()
                    ->map(function($item, $key){return $item->petType;});
      $userPetTypeIds = $userPetTypes->pluck('id')->toArray();
      $parentPetTypeIds = array();
      foreach($userPetTypes as $petType) {
        $this->getUniqueParents($petType, $parentPetTypeIds, $userPetTypeIds);
      }
      $childPetTypeIds = array();
      foreach($userPetTypes as $petType) {
        $this->getUniqueChildren($petType, $childPetTypeIds, $userPetTypeIds);
      }

      $top10WithUserPetTypes = Article::with('user')->withCount('comments')
      ->whereHas('petTypes', function($query) use($userPetTypeIds) {
        $query->whereIn('pet_type_id', $userPetTypeIds);
      })->latest()->take(10)->get();
      $top10WithUserPetTypeIds = $top10WithUserPetTypes->pluck('id')->toArray();

      $top10WithParentPetTypes = Article::with('user')->withCount('comments')
      ->whereHas('petTypes', function($query) use($parentPetTypeIds) {
        $query->whereIn('pet_type_id', $parentPetTypeIds);
      })->latest()->take(10)->get();
      $top10WithParentPetTypeIds = $top10WithParentPetTypes->pluck('id')->toArray();

      $top10WithChildPetTypes = Article::with('user')->withCount('comments')
      ->whereHas('petTypes', function($query) use($childPetTypeIds) {
        $query->whereIn('pet_type_id', $childPetTypeIds);
      })->latest()->take(10)->get();
      $top10WithChildPetTypeIds = $top10WithChildPetTypes->pluck('id')->toArray();

      $top10Latest = Article::latest()->with('user')->withCount('comments')->take(10)->get();

      //Assign weightings
      $this->assignWeightings($top10WithUserPetTypes, 10);
      $this->assignWeightings($top10WithParentPetTypes, 5);
      $this->assignWeightings($top10WithChildPetTypes, 2);
      $this->assignWeightings($top10Latest, 1);

      //Collect into a single collection ignoring duplicates
      //Should do this in this special order so that the duplicate with the
      //higher weighting is retained
      $articles = $top10WithUserPetTypes;
      $articles = $this->appendArticlesToCollectionWithoutDuplicates($articles, $top10WithParentPetTypes);
      $articles = $this->appendArticlesToCollectionWithoutDuplicates($articles, $top10WithChildPetTypes);
      $articles = $this->appendArticlesToCollectionWithoutDuplicates($articles, $top10Latest);

      return $articles->sortByDesc('weighting')->values()->take(10);
    }

    //Assign weightings as weightingFactor/(days old + 1)
    private function assignWeightings($petTypes, $weightingFactor) {
      $now = Carbon::now();
      foreach($petTypes as $petType) {
        $petType->weighting = $weightingFactor/($petType->created_at->diffInDays($now) + 1);
      }
    }

    private function appendArticlesToCollectionWithoutDuplicates($articles, $append) {
      foreach ($append as $toAppend) {
        if (!$articles->contains(function ($value, $key) use ($toAppend) {
            return $value->id === $toAppend->id;
          }))
        {
          $articles->push($toAppend);
        }
      }
      return $articles;
    }
}
