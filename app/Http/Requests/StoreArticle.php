<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreArticle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'content' => 'required',
            'pettype' => 'required'
        ];
    }

    public function messages()
    {
      return [
        'title.required' => 'Please enter a title for the article',
        'title.max' => 'Title must be less than 256 characters',
        'content.required' => 'Please enter some content for the article',
        'pettype.required' => 'Please select at least one pet type'
      ];
    }
}
