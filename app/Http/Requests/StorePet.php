<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePet extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         return [
             'name' => 'required|max:255',
             'pettype' => 'required'
         ];
     }

     public function messages()
     {
       return [
         'name.required' => 'Please enter a name for your pet',
         'name.max' => 'Name must be less than 256 characters',
         'pettype.required' => 'Please select one pet type'
       ];
     }
}
