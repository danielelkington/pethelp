<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

use App\Article;
use App\User;
use App\Mail\NewArticle;

class ProcessNewArticleEmails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $article;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public $tries = 3;
    public $timeout = 120;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      //1. Get article pet Types
      //2. Get users with any pets with any of the pet types mentioned
      //3. Email them!
      $petTypeIdsToSendEmailsFor =
        $this->article->petTypes()
        ->pluck('pet_types.id')->toArray();

      $usersToSendEmailTo =
        User::whereHas('pets', function($query) use ($petTypeIdsToSendEmailsFor) {
          $query->whereIn('pet_type_id', $petTypeIdsToSendEmailsFor);
        })->where('is_subscribed', true)
          ->get();

      foreach ($usersToSendEmailTo as $user) {
        if ($user->id === $this->article->user_id)
          continue;
        Mail::to($user)
          ->send(new NewArticle($this->article, $user));
      }
    }
}
