<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    public function petType() {
      return $this->belongsTo('App\PetType');
    }

    public function user() {
      return $this->belongsTo('App\User');
    }
}
