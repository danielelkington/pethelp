#Variables
#Local environment:
SET @passwordHash = '$2y$10$dkwljF3YQGOS08g9oUnQKud7qTcy4CxFoRixocnS1QbSfEWar7z6m';
#Production environment:
#SET @passwordHash = '$2y$10$.lqO1oqt93ZPgihk5JYOo.UfDsrDPggzZXkNwP9lI5gUJaOr6Z12G';

#Reset database
DELETE FROM article_pet_type;
DELETE FROM articles;
DELETE FROM pets;
DELETE FROM users;

#Add users
INSERT INTO users(name, is_subscribed, email, password) VALUES
('Jane', 0, 'jane@me.com', @passwordHash),
('Ben', 0, 'ben@me.com', @passwordHash),
('Germaine', 0, 'germaine@me.com', @passwordHash);

#Add pets
INSERT INTO pets(name, user_id, pet_type_id) VALUES
('Biscuit', (SELECT Id FROM users WHERE name = 'Jane'), (SELECT Id FROM pet_types WHERE name = 'Labrador Retriever')),
('Jeremy', (SELECT Id FROM users WHERE name = 'Jane'), (SELECT Id FROM pet_types WHERE name = 'Guinea Pig')),
('Carmen', (SELECT Id FROM users WHERE name = 'Jane'), (SELECT Id FROM pet_types WHERE name = 'Gecko')),
('Samuel', (SELECT Id FROM users WHERE name = 'Jane'), (SELECT Id FROM pet_types WHERE name = 'Snake')),
('Hashan', (SELECT Id FROM users WHERE name = 'Ben'), (SELECT Id FROM pet_types WHERE name = 'Poodle')),
('Crystal', (SELECT Id FROM users WHERE name = 'Ben'), (SELECT Id FROM pet_types WHERE name = 'Shih Tzu')),
('Diana', (SELECT Id FROM users WHERE name = 'Ben'), (SELECT Id FROM pet_types WHERE name = 'Dalmation')),
('Han Zong', (SELECT Id FROM users WHERE name = 'Ben'), (SELECT Id FROM pet_types WHERE name = 'Bird')),
('Cherish', (SELECT Id FROM users WHERE name = 'Germaine'), (SELECT Id FROM pet_types WHERE name = 'Dog')),
('Adrian', (SELECT Id FROM users WHERE name = 'Germaine'), (SELECT Id FROM pet_types WHERE name = 'Cat')),
('Andrew', (SELECT Id FROM users WHERE name = 'Germaine'), (SELECT Id FROM pet_types WHERE name = 'Ackie')),
('Ming Ming', (SELECT Id FROM users WHERE name = 'Germaine'), (SELECT Id FROM pet_types WHERE name = 'Chameleon'));

#Add articles
INSERT INTO articles(user_id, created_at, title, content)
VALUES ((SELECT Id FROM users WHERE name = 'Jane'), DATE_SUB(now(), INTERVAL 9 DAY), 'How to feed a Rattlesnake', '<p><img src="https://i.imgur.com/Mu40xD7.jpg" alt="A rattlesnake" class="img-fluid"></p>  <p><strong>Warning: Rattlesnakes are deadly! </strong>In order to stop your pet rattlesnake from attacking you, you''ll want to remain friends with it by feeding it regularly. Rattlesnakes love to eat</p>  <ul><li>Insects</li><li>Mice</li><li>Small rabbits</li><li>Rats</li></ul>  <p>It''s not a good idea to feed your Rattlesnake a live animal, as it will spend time thrashing about in its enclosure attacking the animal, and will learn how to kill. Instead, buy pre-killed, frozen animals, and let them defrost before giving them to your snake.</p>  <p>Above all, keep safe! And don''t let your Rattlesnake get too hungry...</p>  <p>Source: <a href="http://animals.mom.me/how-to-care-for-a-pet-rattlesnake-8512160.html" title="How to care for a pet rattlesnake">animals.mom.me</a></p>');
SET @lastarticleid = last_insert_id();
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Rattlesnake'));
INSERT INTO comments(article_id, user_id, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Germaine'), 'Better advice - don''t keep rattlesnakes as pets!');
INSERT INTO comments(article_id, user_id, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Jane'), 'As a professional pet owner who has kept many rattlesnakes as pets in the past and grown very fond of them, I find that remark very insensitive and highly offensive.');

INSERT INTO articles(user_id, created_at, title, content)
VALUES ((SELECT Id FROM users WHERE name = 'Jane'), DATE_SUB(now(), INTERVAL 7 DAY), 'Keeping snakes warm', '<p><img src="https://i.imgur.com/J4NRh4e.jpg" alt="A snake sunbaking" class="img-fluid"><br></p>  <p>Unlike humans, snakes are cold-blooded, so they don''t regulate their temperature in the same way that people do. Snakes in the wild have to live in environments where there is heat, and have to spend lots of time lying out in the sun to warm their bodies.</p>  <p>There are a number of ways you can help your pet snake to stay warm:</p>  <h3>Heat Lamps</h3>  <p>Heat lamps are lamps you can put in your snake''s tank, which allow your snake to regulate its temperature in its own natural way.</p>  <h3>Chemical Heat Packs</h3>  <p>You can buy these in sporting goods stores. By rubbing or shaking them they''ll heat up, and will last a few hours. Don''t put these directly against a snake''s skin, instead wrap it in a few pieces of foil and tape it to the side of the snake''s skin.</p>  <h3>Hot Water</h3>  <p>Put a jug of hot water into the snake''s tank - it will release vapour that will heat up the tank. This won''t last few long though, only about 15 minutes before you''ll need to change it.</p>  <h3>Car heater</h3>  <p>It''s possible to take the heater out of your car and put it near the snake to keep it warm.</p>  <p>Let me know in the comments if you''ve discovered any other great ways to keep your pet snake warm!</p>  <p>Source: <a href="http://pet-snakes.com/keep-pet-snake-warm" title="Keeping Snakes Warm">Keeping Snakes Warm</a></p>');
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(last_insert_id(), (SELECT Id FROM pet_types WHERE name = 'Snake'));

INSERT INTO articles(user_id, created_at, title, content)
VALUES ((SELECT Id FROM users WHERE name = 'Ben'), DATE_SUB(now(), INTERVAL 8 DAY), 'How to stop pet snakes from biting you', '<p><img src="https://i.imgur.com/zWgx6gt.jpg" alt="Snake with mouth open" class="img-fluid"><br></p>  <p>Even the friendliest snake breeds have been known to bite humans, especially if they''re feeling cross! Here are the top five tips to avoid being bitten by your pet snake:</p>  <ol><li>Don''t offer your pet snake food directly from your hand - it might not know the difference between your hand and the food!</li><li>Keep it well fed - if it''s hungry it might try to look for food itself...and it might find you!</li><li>Approach the snake slowly when touching it. If you move quickly it might see you as a threat and want to fight back!</li><li>Handle your snake gently. Again, stay on its good side.</li><li>Don''t handle it when it is mid-shed - it might feel more vulnerable at this time and might therefore be more inclined to bite you.</li></ol><p>Let me know how you stop your pet snake from biting you in the comments!<br></p>  <p>Source: <a href="https://i.imgur.com/zWgx6gt.jpg">petmd</a></p>');
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(last_insert_id(), (SELECT Id FROM pet_types WHERE name = 'Snake'));

INSERT INTO articles(user_id, created_at, title, content)
VALUES ((SELECT Id FROM users WHERE name = 'Ben'), DATE_SUB(now(), INTERVAL 2 DAY), 'How often should you walk your dog?', '<p><img src="https://i.imgur.com/3TSYafm.jpg" alt="Walking a dog" class="img-fluid"><br></p>  <p>You might have heard of the 15/2 rule:</p>  <blockquote>Fifteen minutes, twice a day</blockquote>  <p>However it actually depends a lot on the dog. Factors affecting this are:</p>  <ul><li>How big is the dog?</li><li>What is the dog breed?</li><li>How busy is the owner?</li><li>Is your dog unwell?</li><li>What is your dog''s diet?</li><li>How much energy does your dog have?</li></ul>  <p>Read more about how often you should walk your dog at the <a href="http://www.dogingtonpost.com/how-often-should-one-walk-their-dog/">dogington post</a>.</p>');
SET @lastarticleid = last_insert_id();
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dog'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Labrador Retriever'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cocker Spaniel'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Springer Spaniel'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'German Shepherd'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Staffordshire Bull Terrier'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cavalier King Charles Spaniel'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Golden Retriever'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'West Highland White Terrier'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Yorkshire Terrier'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Boxer'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Beagle'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dachshund'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Poodle'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Miniature Schnauzer'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Shih Tzu'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Chihuahua'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Bulldog'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Pug'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dalmation'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Great Dane'));
INSERT INTO comments(article_id, user_id, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Jane'), 'I think at least an hour a day is best - if you don''t have time you can check out my dog walking business!');

INSERT INTO articles(user_id, created_at, title, content)
VALUES ((SELECT Id FROM users WHERE name = 'Ben'), DATE_SUB(now(), INTERVAL 6 DAY), 'Stopping Cockatiels from destroying furniture', '<p><img src="https://i.imgur.com/g6i8G80.jpg" alt="Crackers" class="img-fluid"><br></p>  <p>Cockatiels can be destructive! There have been many times when my pet cockatiel Crackers has went and gotten his beak into the bookshelf, putting holes in it and being a nuisance in general. I''ve discovered a few tips to help stop this though:</p>  <ol><li>Give your cockatiel something else to chew on - a piece of paper (without too much ink), cuttlefish, dried raw pasta, etc.</li><li>Monitor your bird carefully, and gently take it away from something it''s trying to chew (that it''s not allowed to chew) and say "No".</li><li>Spend lots of time playing; it won''t be so bored and will chew less.</li></ol>');
SET @lastarticleid = last_insert_id();
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(last_insert_id(), (SELECT Id FROM pet_types WHERE name = 'Cockatiel'));
INSERT INTO comments(article_id, user_id, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Jane'), 'Ooh! Love the dried pasta idea!');
INSERT INTO comments(article_id, user_id, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Germaine'), 'Now I want to get a cockatiel...');

INSERT INTO articles(user_id, created_at, title, content)
VALUES ((SELECT Id FROM users WHERE name = 'Ben'), DATE_SUB(now(), INTERVAL 4 DAY), 'Pets that go well together', '<p><img src="https://i.imgur.com/v5VNbyM.jpg" alt="Many pets" class="img-fluid"></p>  <p>You might be wondering if it''s possible to keep pets of different species. In general it is, and many small animals get along great with others (especially if they grew up together from when they were babies) but there are a few exceptions, and you have to be very careful in some circumstances, especially when in the wild the animals are natural predators and prey. In general, avoid:</p>  <ul><li>Cats and fish</li><li>Cats and birds</li><li>Cats and mice/rats</li><li>Various breeds of birds in a small cage</li><li>Rabbits and guinea pigs - rabbits have been known to bully guinea pigs</li><li>Ferrets and birds</li><li>Snakes and anything</li></ul>  <p>For more information on different pet combinations, go to <a href="http://vetaround.com.au/blog/can-different-species-of-pets-live-together/">vetaround</a>.</p>');
SET @lastarticleid = last_insert_id();
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dog'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Labrador Retriever'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cocker Spaniel'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Springer Spaniel'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'German Shepherd'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Staffordshire Bull Terrier'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cavalier King Charles Spaniel'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Golden Retriever'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'West Highland White Terrier'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Yorkshire Terrier'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Boxer'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Beagle'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dachshund'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Poodle'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Miniature Schnauzer'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Shih Tzu'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Chihuahua'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Bulldog'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Pug'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dalmation'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Great Dane'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cat'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Maine Coon'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Ragamuffin'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Ragdoll'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Persian'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'American Shorthair'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Abyssinian'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Siamese'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Tonkinese'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Russian Blue'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Bengal'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Scottish Fold'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'British Shorthair'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Siberian'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Exotic'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Birman'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Himalayan'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Nebelung'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Norwegian Forest'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'American Bobtail'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Bombay'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Burmese'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Chartreux'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Chinese Li Hua'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Devon Rex'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Ocicat'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Tabby'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Bird'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Parrot'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Small Parrot'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cockatiel'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Lovebird'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Small Parakeet'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Parrotlet'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Medium Parrot'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Caiques'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Small Conure'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Lorikeet'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Large Parakeet'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Pionus Parrot'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Poicephalus'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Large Parrot'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'African Grey'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Amazon'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cockatoo'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Large Conure'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Eclectus'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Hawk Headed Parrot'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Macaw'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Softbill Bird'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Crow'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Raven'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Mynah'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Toucan'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Small Mammal'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Guinea Pig'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Hamster'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Rabbit'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'American Rabbit'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'American Sable'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Angora Rabbit'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Belgian Hare'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Beveren'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Blanc De Hotot'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Californian Rabbit'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cashmere Lop Rabbit'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dutch Rabbit'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dwarf Lop'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dwart Angora'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Holland Lop'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Rex Rabbit'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Netherland Dwarf'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'French Lop'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Ferret'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Chinchilla'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Mouse'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Rat'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Gerbil'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Pygmy hedgehog'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Sugar glider'));

INSERT INTO comments(article_id, user_id, created_at, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Germaine'), DATE_SUB(now(), INTERVAL 3 DAY), 'Is that photo photoshopped? Or is it evidence of animal cruelty!!??');
INSERT INTO comments(article_id, user_id, created_at, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Ben'), DATE_SUB(now(), INTERVAL 2 DAY), 'Dunno, I just found it on the net, looks photoshopped to me. By the way you have some very snarky comments.');

INSERT INTO articles(user_id, created_at, title, content)
VALUES ((SELECT Id FROM users WHERE name = 'Ben'), DATE_SUB(now(), INTERVAL 3 DAY), 'Keeping your dog looking good', '<p><img src="https://i.imgur.com/REVHvqj.jpg" alt="Good Boy" class="img-fluid"><br></p>  <p>There are many advantages to keeping your dog looking good - you can enter them in shows and maybe make some money, and it''s always fun to have a more instagram-worthy pet. My top tips for keeping your dog looking good are:</p>  <ul><li>Wash it regularly</li><li>Dress it up - there are many cute dog costumes!</li><li>Keep it brushed clean</li><li>Reward it when it smiles - this will teach it to smile regularly.</li></ul>  <p>Let me know in the comments what your top tips are!</p>');
SET @lastarticleid = last_insert_id();
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dog'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Labrador Retriever'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cocker Spaniel'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Springer Spaniel'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'German Shepherd'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Staffordshire Bull Terrier'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cavalier King Charles Spaniel'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Golden Retriever'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'West Highland White Terrier'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Yorkshire Terrier'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Boxer'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Beagle'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dachshund'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Poodle'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Miniature Schnauzer'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Shih Tzu'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Chihuahua'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Bulldog'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Pug'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dalmation'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Great Dane'));

INSERT INTO comments(article_id, user_id, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Germaine'), 'That''s my dog! This photo is copyrighted');

INSERT INTO articles(user_id, created_at, title, content)
VALUES ((SELECT Id FROM users WHERE name = 'Ben'), DATE_SUB(now(), INTERVAL 4 DAY), 'Teaching large parrots to talk', '<p><img src="https://i.imgur.com/lj34XDv.jpg" alt="An Eclectus parrot" class="img-fluid"></p>  <p>If you want your large parrot to talk, a key point is to begin training it when it is as young as possible, especially for Amazon parrots which typically will only learn words and phrases up until they''re about 1 year old, and will not learn anything after that.</p>  <p>Although large parrots won''t be developed enough to begin speaking until they''re at least 6 months old, they''ll learn to listen and recognise words from when they''re very tiny, so start talking to them from when they''re very young. Do <em>not</em> whistle to birds that cannot yet talk - whistling is relatively easy for them to mimic, and they''ll be so excited from whistling they won''t want to try the tougher challenge of talking!</p>  <p>Make sure you have a good relationship with your parrot. When training, try to eliminate distractions like TVs, music and phones. It should only be you and the bird in the room. The bird will be more interested in how you say the word rather than the word itself, so say a word in a loud clear voice with emotion and gusto, pause, then repeat. And repeat. And repeat.</p>  <p>Training is best done in the morning and evening. Train no longer than 15 minutes in a single session. Only work on one word or phrase at a time until it is learned well. Do not make looped recordings - the parrot is more interested if it sees a human saying the words and it feels like it is interacting with a person, it won''t want to interact with a dumb machine.</p>  <p>For more tips check out <a href="https://www.2ndchance.info/parrottalk.htm" title="Parrot Talk">2ndchance</a>.</p>');
SET @lastarticleid = last_insert_id();
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Large Parrot'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'African Grey'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Amazon'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cockatoo'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Large Conure'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Eclectus'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Hawk Headed Parrot'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Macaw'));

INSERT INTO comments(article_id, user_id, created_at, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Germaine'), DATE_SUB(now(), INTERVAL 3 DAY), 'Great tips Ben - now I want a parrot so I can give it a go. What parrot would you recommend for its talking ability?');
INSERT INTO comments(article_id, user_id, created_at, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Ben'), DATE_SUB(now(), INTERVAL 2 DAY), 'African Grey parrot, but it''s the parrot that needs the most attention, and you''ll need to be someone who can spend many hours a day (virtually all the time) with it. Otherwise there are birds who don''t need quite as much attention, such as a Cockatoo.');


INSERT INTO articles(user_id, created_at, title, content)
VALUES ((SELECT Id FROM users WHERE name = 'Germaine'), DATE_SUB(now(), INTERVAL 4 DAY), 'Clipping small parrot wings', '<p><img src="https://i.imgur.com/zBaRhDC.jpg" alt="Small parrot" class="img-fluid"><br></p>  <p>There''s some debate about whether you should clip your small parrot''s wings, but personally I''m in favour - I think you should do some minor clipping so that the bird can still fly, just so that it can''t fly too fast or too far, meaning that it''s less likely to crash, and meaning it expands more energy as it flies.</p>  <p>Don''t clip baby parrot wings, as this can stunt the regular growth of the feathers. Instead, wait until the bird can fly before clipping. If you don''t feel confident doing so or you''ve never done it before, get your vet to do it, see if it works for you, and then decide if you want to continue doing it yourself.</p>  <p>Clipping the bird''s wings is best done with two people - at least must be someone who the bird trusts a lot and will allow it to be held tightly. Drape a towel over the bird, pull a wing out of the towel restraint, and find the primary flight feathers - the last ten and longest feathers on the wing. Cut the outer 6 or 7, leaving a few centimetres between the cut end of the feather and the smaller feathers above.</p>  <p>For more details, including a video and diagrams, check out <a href="https://www.wikihow.com/Clip-a-Parrot%27s-Wings" title="Clipping a parrot''s wings">the wikihow page</a> on the topic.</p>');
SET @lastarticleid = last_insert_id();
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Small Parrot'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cockatiel'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Lovebird'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Small Parakeet'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Parrotlet'));

INSERT INTO comments(article_id, user_id, created_at, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Jane'), DATE_SUB(now(), INTERVAL 3 DAY), 'A bird should naturally fly; I''d never clip my bird''s wings.');

INSERT INTO articles(user_id, created_at, title, content)
VALUES ((SELECT Id FROM users WHERE name = 'Germaine'), DATE_SUB(now(), INTERVAL 3 DAY), 'How often to feed your pet fish', '<p><img src="https://i.imgur.com/W54GlhO.jpg" alt="A goldfish eating" class="img-fluid"></p>  <p>Most aquarium fish should be fed once a day, although some pet owners prefer to feed their fish twice. Regardless, you should keep each feeding very small. You don''t want your fish to overeat, and you also don''t want the tank or pond to become contaminated with excess food.</p>  <p>a good rule is that your fish should not be eating for more than 5 minutes in a single session - for goldfish 2 minutes is a better rule. It''s better to underfeed rather than overfeed. If your fish finishes all the food in a minute, you can give another small feeding. If you put too much food in, you can fish it out with a small net.</p>  <p>More info <a href="https://www.thesprucepets.com/how-much-and-how-often-should-you-feed-aquarium-fish-1380923" title="the spruce Pets">here</a> and <a href="http://goldfish2care4.com/goldfish-feeding.html" title="Goldfish feeding">here</a> for goldfish.</p>  <p><br></p>');
SET @lastarticleid = last_insert_id();
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Fish'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Goldfish'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Betta fish'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Guppie'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'White Cloud Minnow'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Tetra'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Salt and Pepper Corydoras'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Zebra Danios'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Oscar'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Mollie'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Platie'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Barb'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Gourami'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Swordtail'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Discus'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Killifish'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Bettas'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Plecostomus'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Rainbowfish'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Catfish'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Angelfish'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Plecostomus'));

INSERT INTO comments(article_id, user_id, created_at, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Jane'), DATE_SUB(now(), INTERVAL 2 DAY), 'Thanks for the warning about overfeeding, I didn'' know that!');

INSERT INTO articles(user_id, created_at, title, content)
VALUES ((SELECT Id FROM users WHERE name = 'Germaine'), DATE_SUB(now(), INTERVAL 6 DAY), 'Washing your pet dog', '<p><img src="https://i.imgur.com/H6oeoxL.jpg" alt="Dog getting washed" class="img-fluid"></p>  <p>If you don''t wash your pet dog, it will soon start to smell! Here are my top tips for washing your pet dog.</p>  <ul><li>Don''t use products designed for humans - dog skin is different to human skin</li><li>Wash smaller dogs in the laundry sink or a normal bath tub. Larger dogs can be washed outside, especially on a warm day. Playing a game of fetch or having a walk afterwards will speed up the drying process.</li><li>Keep a hand on the dog at all times while washing, which makes it less likely that they''ll shake all over you or leap from the bath.</li><li>Use food rewards to encourage good behaviour</li><li>If the dog wiggles and gets water in its ears, use cotton wool to loosely plug its ears</li><li>Only use a very small amount of shampoo, enough to cover a medium size coin</li><li>Massage your dog all over with the shampoo, leave on for the recommended amount of time. Don''t get shampoo on the dog''s face- - use a face washer or sponge to tip the face without shampoo.</li><li>Rinse thoroughly.</li><li>First let the dog shake itself dry, then use a towel. Finally use a hairdryer on a low heat or take the dog for a walk in the sun.</li></ul>  <p>If the dog keeps smelling after washing, it might be sick or have other problems. Take it to the vet to get it checked.</p>  <p>Source: <a href="https://www.vetbabble.com/dogs/grooming-dogs/head-to-toe-dog-washing-guide/" title="Dog Washing Guide">vetbabble</a></p>');
SET @lastarticleid = last_insert_id();
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dog'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Labrador Retriever'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cocker Spaniel'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Springer Spaniel'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'German Shepherd'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Staffordshire Bull Terrier'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cavalier King Charles Spaniel'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Golden Retriever'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'West Highland White Terrier'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Yorkshire Terrier'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Boxer'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Beagle'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dachshund'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Poodle'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Miniature Schnauzer'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Shih Tzu'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Chihuahua'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Bulldog'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Pug'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dalmation'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Great Dane'));

INSERT INTO comments(article_id, user_id, created_at, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Ben'), DATE_SUB(now(), INTERVAL 2 DAY), 'That''s my dog! This photo is copyrighted');

INSERT INTO articles(user_id, created_at, title, content)
VALUES ((SELECT Id FROM users WHERE name = 'Germaine'), DATE_SUB(now(), INTERVAL 5 DAY), 'Best cat food brands', '<p><img src="https://i.imgur.com/mBDciqP.jpg" alt="Cat with some food" class="img-fluid"></p>  <p>There are so many brands of cat food on the market, it''s hard to know which is best. Of course, it depends a bit on your cat. Take a look at the packet or tin, and make sure that it has a lot of protein, and doesn''t have artificial additives or low-quality filters.</p>  <p><a href="https://tikipets.com/">Tiki Cat</a> is my favourite brand - my cat loves both the wet and dry variety. It has wholesome, recognisable ingredients, and has so much flavour. Of course, if your cat doesn''t like it, there are a number of other good brands. <a href="https://www.reviews.com/cat-food/">reviews.com</a> has a lot of good recommendations.</p>');
SET @lastarticleid = last_insert_id();
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cat'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Maine Coon'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Ragamuffin'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Ragdoll'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Persian'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'American Shorthair'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Abyssinian'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Siamese'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Tonkinese'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Russian Blue'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Bengal'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Scottish Fold'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'British Shorthair'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Siberian'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Exotic'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Birman'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Himalayan'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Nebelung'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Norwegian Forest'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'American Bobtail'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Bombay'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Burmese'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Chartreux'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Chinese Li Hua'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Devon Rex'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Ocicat'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Tabby'));

INSERT INTO comments(article_id, user_id, created_at, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Ben'), DATE_SUB(now(), INTERVAL 1 DAY), 'Why buy when you can cook yourself?');
INSERT INTO comments(article_id, user_id, created_at, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Germaine'), DATE_SUB(now(), INTERVAL 0 DAY), 'Not everyone has time for that Ben ;)');


INSERT INTO articles(user_id, created_at, title, content)
VALUES ((SELECT Id FROM users WHERE name = 'Germaine'), DATE_SUB(now(), INTERVAL 5 DAY), 'How to build a rabbit hutch', '<p><img src="https://i.imgur.com/V2ns21L.jpg" alt="A large rabbit hutch" class="img-fluid"></p>  <p>Building a rabbit hutch is no easy endeavour. You''ll want to have a lot of experience in construction and design. I''m not going to give you a step-by-step guide here (<a href="https://www.wikihow.com/Build-a-Rabbit-Hutch" title="Build a Rabbit Hutch">here''s</a> a good starting point for that). Instead, a few things to consider:</p>  <ol><li>It needs to be big. If you want a happy, healthy rabbit, you''ll want to give it plenty of stretching and hopping space.</li><li>Rabbits are social creatures and are best kept in pairs. So again, you''ll want to make your hutch big.</li><li>While wire floors are easier to clean, they''re not comfortable for the rabbit. Instead get a solid floor, although it will be harder to keep clean</li><li>Make sure the door is wide enough for you to fit your arms <em>and</em> the rabbit through.</li><li>Straw or hay makes good bedding</li><li>Anything plastic will eventually be chewed away by your rabbit.</li></ol>  <p>Have fun building!</p>');
SET @lastarticleid = last_insert_id();
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Rabbit'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'American Rabbit'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'American Sable'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Angora Rabbit'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Belgian Hare'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Beveren'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Blanc De Hotot'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Californian Rabbit'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Cashmere Lop Rabbit'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dutch Rabbit'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dwarf Lop'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Dwart Angora'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Holland Lop'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Rex Rabbit'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'Netherland Dwarf'));
INSERT INTO article_pet_type(article_id, pet_type_id)
VALUES(@lastarticleid, (SELECT Id FROM pet_types WHERE name = 'French Lop'));

INSERT INTO comments(article_id, user_id, created_at, content)
VALUES(@lastarticleid, (SELECT Id FROM users WHERE name = 'Ben'), DATE_SUB(now(), INTERVAL 1 DAY), 'Warning - I tried to build my own and it was a disaster (collapsed to pieces). Maybe I''m just not a good builder, but from now on I''ll always buy a pre-built one.');
